module.exports = app => {
    app.listen(app.get('port'), () => {
        console.log(`server port on ${app.get('port')}`)
    });
};

